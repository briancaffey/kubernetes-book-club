# Resource Limits

## Requesting Resources for a pod's containers

- A pod's resource requests and limits are the sum of all of the pod's container resource requests and limits

### Creating pods with resource requests

### Understanding how resource requests affects scheduling

- the Scheduler doesn’t look at how much of each individual resource is being used at the exact time of scheduling but at the sum of resources requested by the existing pods deployed on the node.

- LeastRequestedPriority (spreading)
- MostRequestedPriority (packing)


### Understanding how CPU requests affect CPU time sharing

```
apiVersion: v1
kind: Pod
metadata:
  name: requests-pod
spec:
  containers:
    - image: busybox
      command: ["dd", "if=/dev/zero", "of=/dev/null"]
      name: main
      resources:
        requests:
          cpu: 200m
          memory: 10Mi
```


```
kubectl run requests-pod-2 \
    --image=busybox \
    --restart Never \
    --requests='cpu=800m,memory=20Mi' \
    -- dd if=/dev/zero of=/dev/null
```

```
kubectl run requests-pod-3 \
    --image=busybox \
    --restart Never \
    --requests='cpu=1,memory=20Mi' \
    -- dd if=/dev/zero of=/dev/null
```

```
k describe pods requests-pod-3
Name:         requests-pod-3
Namespace:    default
Priority:     0
Node:         <none>
Labels:       run=requests-pod-3
Annotations:  <none>
Status:       Pending
IP:
Containers:
  requests-pod-3:
    Image:      busybox
    Port:       <none>
    Host Port:  <none>
    Args:
      dd
      if=/dev/zero
      of=/dev/null
    Requests:
      cpu:        1
      memory:     20Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-hwbgp (ro)
Conditions:
  Type           Status
  PodScheduled   False
Volumes:
  default-token-hwbgp:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-hwbgp
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type     Reason            Age   From               Message
  ----     ------            ----  ----               -------
  Warning  FailedScheduling  15s   default-scheduler  0/1 nodes are available: 1 Insufficient cpu. <-- not enough CPU
```


```
kubectl describe node
```

- any unused CPU will be split among the two pods in a 1 to 5 ratio

- But if one container wants to use up as much CPU as it can, while the other one is sitting idle at a given moment, the first container will be allowed to use the whole CPU time (minus the small amount of time used by the second container, if any).

- When a process tries to allocate memory over its limit, the process is killed (it’s said the container is OOMKilled , where OOM stands for Out Of Memory)


### Defining and requesting custom resources

- `dongle` example from k8s docs: https://kubernetes.io/docs/tasks/administer-cluster/extended-resource-node/
- An example of a custom resource could be the number of GPU units available on the node

## Limiting resources available to a container

### Setting a hard limit for the amount of resources a container can use

- memory is not compressible so it needs limits
- cpu is compressible

- resource limits aren’t constrained by the node’s allocatable resource amounts

### Exceeding the limits

- 20, 40, 80, and 160, then 300 -- every five minutes until the pod stops crashing or is deleted
- describe pod or get logs to see why a pod crashed

- Containers can get OOMKilled even if they aren’t over their limit.

### Understanding how apps in containers see limits

- Even though you set a limit on how much memory is available to a container, the container will not be aware of this limit

- Setting a CPU limit to one core doesn’t magically only expose only one CPU core to the container.

- Certain applications look up the number of CPUs on the system to decide how many worker threads they should run

- You may want to use the Downward API to pass the CPU limit to the container and use it instead of relying on the number of CPUs your app can see on the system

## Understanding pod QoS classes

3 Quality of Service (QoS) classes:
- BestEffort (the lowest priority)
- Burstable
- Guaranteed (the highest)

### Defining the QoS class for a pod

(See tables), if any are Burstable, then Burstable.

### Understanding which process gets killed when memory is low

- When the system is overcommitted, the QoS classes determine which container gets killed first so the freed resources can be given to higher priority pods

- Each running process has an OutOfMemory (OOM) score. The system selects the process to kill by comparing OOM scores of all the running processes. When memory needs to be freed, the process with the highest score gets killed.

OOM Score base on:

- the percentage of the available memory the process is consuming and
- a fixed OOM score adjustment, which is based on the pod’s QoS class and the container’s requested memory.

## Setting default requests and limits for pods per namespace

- It’s a good idea to set requests and limits on every container.

### Introducing the LimitRange resource

- Instead of having to do this for every container, you can also do it by creating a LimitRange resource

- It allows you to specify (for each namespace) not only the minimum and maximum limit you can set on a container for each resource, but also the default resource requests for containers that don’t specify requests explicitly

- LimitRange resources are used by the LimitRanger Admission Control plugin

- The limits specified in a LimitRange resource apply to each individual pod/container or other kind of object created in the same namespace as the LimitRange object. They don’t limit the total amount of resources available across all the pods in the namespace. This is specified through ResourceQuota objects, which are explained in section 14.5.

### Creating a LimitRange object

For type of container:

- defaultRequest
- default
- min
- max
- maxLimitRequestRatio


Exampel of `maxLimitRequestRatio`:

maxLimitRequestRatio to 4, which means a container’s CPU limits will not be allowed to be more than four times greater than its CPU requests

- Limits from multiple LimitRange objects are all consolidated when validating a pod or PVC.

### Enforcing limits

### Applying default resource requests and limits

- But remember, the limits configured in a LimitRange only apply to each individual pod/container. It’s still possible to create many pods and eat up all the resources available in the cluster. LimitRanges don’t provide any protection from that. A ResourceQuota object, on the other hand, does

## Limiting the total resources available in a namespace

### Introducing the ResourceQuota object

- the ResourceQuota Admission Control plugin checks whether the pod being created would cause the configured ResourceQuota to be exceeded

If this is the case, the pod's creation is rejected

- A ResourceQuota limits the amount of computational resources the pods and the amount of storage PersistentVolumeClaims in a namespace can consume.

- limit the number of pods, claims, and other API objects users are allowed to create inside the namespace

```
apiVersion: v1
kind: ResourceQuota
metadata:
    name: cpu-and-mem
spec:
    hard:
        requests.cpu: 400m
        requests.memory: 200Mi
        limits.cpu: 600m
        limits.memory: 500Mi
```

- A ResourceQuota object applies to the namespace it’s created in

- One caveat when creating a ResourceQuota is that you will also want to create a LimitRange object alongside it.

- When a quota for a specific resource (CPU or memory) is configured (request or limit), pods need to have the request or limit (respectively) set for that same resource; otherwise the API server will not accept the pod

### Specifying a quota for persistent storage

- Kubernetes also makes it possible to define storage quotas for each StorageClass individually

? Question

```
spec:
    hard:
        requests.storage: 500Gi
        ssd.storageclass.storage.k8s.io/requests.storage: 300Gi
        standard.storageclass.storage.k8s.io/requests.storage: 1Ti
```

### Limiting the number of objects that can be created

- Pods
- ReplicationControllers
- Secrets
- ConfigMaps
- PersistentVolumeClaims
- Services

### Specifying quotas for specific pod states and/or QoS classes

Quotas can also be limited to a set of quota scopes.

BestEffort
NotBestEffort
Terminating
NotTerminating

## Monitoring pod resource usage

### Collecting and retreiving actual resource usages

### Storing and analyzing historical resource consumption statistics
