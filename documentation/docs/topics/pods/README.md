# Pods

## An introduction to pods

Pods are the central, most important concept in Kubernetes.

Everything else in Kubernetes interacts with Pods in some way.

Let's look at some definitions of the word `pod`:

> a small herd or school of marine animals, especially whales.

::: tip Example
>a pod of four whales entered the bay

# :ocean: :whale: :whale: :whale: :whale: :ocean:
:::

Pods

Kubernetes documentation

> A Pod is the basic execution unit of a Kubernetes application–the smallest and simplest unit in the Kubernetes object model that you create or deploy. A Pod represents processes running on your Cluster

::: tip Example
> There are over 500 pods in my kubernetes cluster
:::

### Motivation: Why do we need pods?

Pods *can* run multiple containers, but often times pods only run one container.

If you run multiple process in a single container, you need to do more work:

- Managing logs for each process
- Make sure the different processes keep running

Containers are designed to run one process per container. Pods allow us to easily run multiple containers in a given pod.

- These containers will always run on the same node. A pod's containers cannot be split between nodes.

When a pod runs multiple containers, those containers are usually closely related. This allows them to run *as if* they were in the same container, but provides a certain level of isolation.

Here's an example from the docker documentation showing a pod running multiple containers:

<base-image width="200px" image="pod.svg" />

The main process here is a `Web Server`, and the `File Puller` container is running in the same pod. The `File Puller` container is sometimes called a "sidecar container".

Other examples of pods with multiple containers include:

- A container that is responsible for monitoring another container
- Wrapping a service in HTTPS using something like nginx or Traefik

### Isolation of Containers in the same pod

Containers that run on the same pod have "partial isolation". They share the same Network and UTS Namespaces (Unix Time Sharing Namespace)

- They don't share the same Process ID Namespace by default, but this can be set.
- Filesystems of two containers in a pod are **totally isolated** (but files can be shared using volumes, another kubernetes resource)
- Containers on two different pods can never have port conflicts, but **containers on the same pod can have port conflicts**

### Flat Inter-Pod Network

> All pods in a kubernetes cluster reside in a single flat, shared, network address space.

- Any pod can access any other pod by the pod's IP address
- No network address translation gateway is needed for communication between pods
- When `podA` sends a packet to `podC`, the source IP of `podA` will be visible to `podC`

```
        |        |          | container_1
        |        |   podA   |
        |        | 10.1.1.6 |
        |        |          | container_2
        | node1  |
        |        |          | container_1
        |        |   podB   |
        |        | 10.1.1.7 |
        |        |          | container_2
flat    |
network |
        |        |          | container_1
        |        |   podC   |
        |        | 10.1.2.5 |
        |        |          | container_2
        | node2  |
        |        |          | container_1
        |        |   podD   |
        |        | 10.1.2.7 |
        |        |          | container_2
```

Because of this, communication between pods is **simple**.

> pods are logical hosts and behave
much like physical hosts or VMs in the non-container world

## Creating pods from YAML and JSON files

Pods can be created by makeing a `POST` request to the Kubernetes REST API. This can be done using the `kubectl run` command, or by running `kubectl create -f mypod.yaml`.

Using the `kubectl create` or `kubectl apply` commands allow for specifying lots of options in the pod definition.

::: warning Pods are generally not created directly
Higher-level resources such as **Deployments** are used to create pods, but a **Pod** resource can be created directly. Higher-level constructs allow for lots of other features, such as scaling, health checks, etc.
:::

Here's how to create a pod directly:

```bash
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: busybox
    command: ['sh', '-c', 'echo Hello Kubernetes! && sleep 3600']
```

This basic example includes:

- the version of the API to be used: `v1`
- the type of resource being created: `Pod`
- metadata
- spec: defines a **list** of containers

The `spec` portion would usually be included as a nested key in another `YAML` file that defines a `Replication Controller` such as a `Deployment`.

Let's test this out with minikube:

```
minikube start
```

Let's define a simple Pod that runs one container (`nginx`):

This file is located in `examples/pods/my-pod.yml`:

```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: nginx:alpine
```

Let's create this pod with:

```
$ kubectl create -f examples/pods/my-pod.yml
pod/myapp-pod created
```

Now let's check to see that the pod is there with:

```
$ kubectl get pods
NAME        READY   STATUS    RESTARTS   AGE
myapp-pod   1/1     Running   0          5m31s
```

We don't have a `Service` defined yet, so we will have setup port-forwarding in order to send requests to the container. Then we can check the container's logs.

```
kubectl port-forward myapp-pod 8888:80
Forwarding from 127.0.0.1:8888 -> 80
Forwarding from [::1]:8888 -> 80
```

From another terminal winodw, let's `curl` the container:

```
$ curl localhost:8888
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

Now let's check the logs of the container:

```
kubectl logs myapp-pod
127.0.0.1 - - [24/Aug/2019:20:48:27 +0000] "GET / HTTP/1.1" 200 612 "-" "curl/7.58.0" "-"
```

## Organizing pods with labels :label:

As we get more pods in our cluster, we need a better way to organize them.

- Labeling pods can give us multi-dimensional organization of pods, such as having one label for the app (`frontend`, `backend`, etc.), and another for the release (`staging`, `production`, `canary`, `development` etc.)

## Constrain pod scheduling with labels and selectors

Pod placement is random. For certain applications, some pods may be required to run on certain nodes with special characteristics such as

- SSD
- GPU

::: tip Coupling
This *couples* the application to the infrastructure, which is generally against the idea of Kubernetes
:::

A pod can be scheduled to a *specific* node using the `kubernetes.io/hostname` label. However, the pod might not be able to be sceduled if the node is not online.

We have alreay seen one of the most important uses of labels: indicating which pods in a `ReplicationController` should serve requests for give `Service`. Labels are referenced in both the `Service` definition and the `ReplicationController` definition.

## Pod annotations

Pods and other kubernetes resources can also use *annotations*. These are slightly different from labels.

- The don't contain *identifying information*
- You can't select objects by annotations
- Annotations are often created automatically be tools
- Annotations can hold much larger amounts of data
- Used by kubernetes to introduce new features
- An annotation might include the name of the person who created the resource to facilitate collaboration on big teams

::: tip Namespace your annotations
Since annotations can be added automatically by other tools and libraries, it is a good idea to prepend a namespace to an annotation key to prevent conflicts
:::

An example of this would be: `my-namespace/my-annotation="some annotation value"`

## Grouping resources with namespaces

There is another method of organization that is of a higher level than labels called `namespaces`. Namespaces in kubernetes are differnt from Linux namespaces. In kubernetes, namespaces can be used to create "virtual clusters" by creating isolation between two different groups of resources.

Not *all* resources exist in a namespace. For example, kubernetes `node` objects do not belong to a namespace.

Without specify a namespace, a resource will be placed into the `default` namespace. Let's list all of the namespaces in our local minikube cluster:

```
kubectl get ns
NAME              STATUS   AGE
default           Active   87m
kube-node-lease   Active   87m
kube-public       Active   87m
kube-system       Active   87m
```

::: tip Kubernetes runs on kubernetes
Most (if not all) components of kubernetes are run by kubernetes itself. We can see this by listing pods in the `kube-system` namespace.

```
kubectl get po --namespace kube-system
NAME                                    READY   STATUS    RESTARTS   AGE
coredns-5c98db65d4-c9fsn                1/1     Running   0          97m
coredns-5c98db65d4-jbrrf                1/1     Running   0          97m
etcd-minikube                           1/1     Running   0          96m
kube-addon-manager-minikube             1/1     Running   0          96m
kube-apiserver-minikube                 1/1     Running   0          96m
kube-controller-manager-minikube        1/1     Running   0          96m
kube-proxy-fdsmv                        1/1     Running   0          97m
kube-scheduler-minikube                 1/1     Running   0          96m
kubernetes-dashboard-7b8ddcb5d6-ngv8z   1/1     Running   0          97m
storage-provisioner                     1/1     Running   0          97m
```

We can see some of the core components of kubernetes, such as `controller-manager`, `apiserver`, `scheduler` and `kube-proxy`.
:::


Namespaces can also provide:

- Separation of different environments: `staging`, `production`, `development`. Keep in mind that we also saw this with labels
- Separation and limits of compute, memory and other system resources by namespace
- Access control for different groups of users. Different teams might get access to a namespace in the cluster that has some limit on cluster resources.

You can create a namespace imperatively with:

```
kubectl create namespace custom-namespace
```

Or you can create them declaratively with YAML:

```
apiVersion: v1
kind: Namespace
metadata:
    name: custom-namespace
```

You would tell Kubernetes to create this namespace by running:

```
$ kubectl create -f custom-namespace.yaml
```

## Stopping and removing pods

There are a few options for deleting pods. We can delete a pod by name:

```
$ kubectl delete po myapp-pod
```

::: warning Stopping containers in a pod gracefully
Kubernetes sends a `SIGTERM` signal to the process and waits a certain
number of seconds (30 by default) for it to shut down gracefully. If it doesn’t shut down in time, the process is then killed through `SIGKILL`.
:::

If this Pod was create by a `ReplicationController`, it would be recreated after it is deleted.

We can also delete pods by label selectors:

```
$ kubectl delete po -l rel=canary
```

Or delete all pods in a namespace by deleting the namespace:

```
$ kubectl delete ns custom-namespace
```

Read more about pods in the [Kubernetes documentation](https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/):

- Init containers
- Pod stages:

|Status | Description |
|-----|-----|
|Pending | The Pod has been accepted by the Kubernetes system, but one or more of the Container images has not been created. This includes time before being scheduled as well as time spent downloading images over the network, which could take a while.|
|Running | The Pod has been bound to a node, and all of the Containers have been created. At least one Container is still running, or is in the process of starting or restarting.|
|Succeeded | All Containers in the Pod have terminated in success, and will not be restarted.|
|Failed | All Containers in the Pod have terminated, and at least one Container has terminated in failure. That is, the Container either exited with non-zero status or was terminated by the system.|
|Unknown | For some reason the state of the Pod could not be obtained, typically due to an error in communicating with the host of the Pod.|


