# Securing the API Server

This section will cover:

- Authentication
- ServiceAccounts
- RBAC Plugin
- Roles and RoleBindings
- ClusterRoles RoleBindings
- Defuault Roles and RoleBindings

## Authentication

In Chapter 8 we used the ServiceAccount token mounted into the pod to authenticate with the Kubernetes API server.

This section will cover how to configure **ServiceAccounts**

> When a request is sent to the API server, it goes through a series of authentiction plugins in order to determine who is sending the request

The first plugin that gets the

- username
- user ID
- groups the client belongs to

Returns the information and continues onto the authorization plugins.

**Authorization plugins use the folowing methods:

- Client Certificate
- Authentication token passed in HTTP Header
- Basic HTTP authentication
- Others... (what others?)


### Users and Groups

User and group info from a request is not stored anywhere, it is used to determine if a user is authorized to perform an action or not

Two kinds of users:

1. Actual human beings
1. Pods (applications running inside them)

Users use external system such as SSO, but pods use **service accounts** which are stored in Kubernetes as `ServiceAccount` resources.

::: tip No user account resources
There are no Kubernetes resources for users. The focus here will be on ServiceAccounts because they are essential for running pods
:::

Humans users and ServiceAccounts can belong to one or more groups, groups grant several permissions at once, analagous to AWS IAM Groups

There are some **built-in groups** with special meanings:

- `system:unauthenticated`: group for requests that could not be authenticated by any plugin
- `system:authenticated`: group for successfully authenticated user
- `system:serviceaccounts`: all ServiceAccounts in the system
- `system:serviceaccounts:<namespace>`: all ServiceAccounts in a namespace

### Service Accounts

Pods can authenticate by sending the contents of:

```
/var/run/secrets/kubernetes.io/serviceaccount/token
```

A volume that is mounted in each container's file system as a `Secret`.

`ServiceAccount` represents the identity of the app running in the pod. This token file holds the `ServiceAccount`'s authentication token.

The authentication plugin sees this token and passes the ServiceAccount's username to the API. ServiceAccount usernames look like this:

```
system:serviceaccount:<namespace>:<service account name>
```

The token is a JSON Web Token (JWT):

```
k describe secret default-token-6hbdw
Name:         default-token-6hbdw
Namespace:    default
Labels:       <none>
Annotations:  kubernetes.io/service-account.name: default
              kubernetes.io/service-account.uid: 89e716f2-6e8e-4a40-9c13-940cc6b55b7a

Type:  kubernetes.io/service-account-token

Data
====
ca.crt:     1066 bytes
namespace:  7 bytes
token:      eyJhbGciOiJSUzI1NiIsImtpZCI6Ikt5d2xSaTI4eWczZW4xQjNxNGFJdFRZbWh2RFhmdlByUlQxSzVpcV81U1EifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImRlZmF1bHQtdG9rZW4tNmhiZHciLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZGVmYXVsdCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6Ijg5ZTcxNmYyLTZlOGUtNGE0MC05YzEzLTk0MGNjNmI1NWI3YSIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDpkZWZhdWx0OmRlZmF1bHQifQ.lxknDwttVyclpk2_TNZNSKGrB8ZN35in95N4xLH0ix7t2Z3W7nBLLkMqqQ-2_Q-BE3ycXS4J6OlVsZ6DJVrjNXh1S9aSGnypEeWBjJ4vXRe0nJ7wKoQOk_ffNU3rYmR4cx5Mv2MaSwNNex30SDar04qxqgrNnnPr6EEqsBt9NqE_WrnaMBhe_KGlcDMwaCSjGWhUhub6guR0HW6PWlW1POgKMOxVoDIoU5fj-A6qfPOPgcM6O5vrtFj1AQa9gpssGZvW4fNVAHzb4nt7kw2rvjW67mM2Y5A5Ddb7yXAj_TI2IOB0EdlJPtniNmr7lB4vc5g_IQ1GHB4yZK_Ur6B1Cg
```

Let's examine the token's payload:

```bash
echo eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZX
J2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImRlZmF1bHQtd
G9rZW4tNmhiZHciLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZGVmYXVsdCIsImt1YmVybmV0ZXMu
aW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6Ijg5ZTcxNmYyLTZlOGUtNGE0MC05YzEzLTk0MGNjNmI1NWI3YSIsInN1YiI6InN
5c3RlbTpzZXJ2aWNlYWNjb3VudDpkZWZhdWx0OmRlZmF1bHQifQ | base64 --decode | jq
```

```json
{
  "iss": "kubernetes/serviceaccount",
  "kubernetes.io/serviceaccount/namespace": "default",
  "kubernetes.io/serviceaccount/secret.name": "default-token-6hbdw",
  "kubernetes.io/serviceaccount/service-account.name": "default",
  "kubernetes.io/serviceaccount/service-account.uid": "89e716f2-6e8e-4a40-9c13-940cc6b55b7a", <--id
  "sub": "system:serviceaccount:default:default" <-- username
}
```

The username is then passed to the authorization plugins to determine if the given user is allowed to perform the requested action.

> Each pod is associated with exactly one **ServiceAccount**, but multiple pods can use the same **ServiceAccount**.

#### Service Accounts and Authorization

A service account can be specified in the Pod manifest. If no ServiceAccount is specified, the default ServiceAccount in that Pod's namespace is used.

**This controls which resources each pod has access to**. Again, authentication gets the username, the API then checks to see if the user is authorized to do what the user is trying to do usin the system-wide authorization plugin configured by the administrator. `RBAC` is one such authorization plugin.

:question: What is the authorization plugin that minikube uses?

[https://stackoverflow.com/a/821889](https://stackoverflow.com/a/821889)

```
kubectl exec kube-apiserver-minikube -it -n kube-system -- sh
# cat /proc/1/cmdline | sed -e "s/\x00/ /g"; echo
kube-apiserver \
    --advertise-address=192.168.99.100 \
    --allow-privileged=true \
    --authorization-mode=Node,RBAC \ <-- Node and RBAC
    --client-ca-file=/var/lib/minikube/certs/ca.crt \
    --enable-admission-plugins=NamespaceLifecycle,LimitRanger,ServiceAccount,DefaultStorageClass,DefaultTolerationSeconds,NodeRestriction,MutatingAdmissionWebhook,ValidatingAdmissionWebhook,ResourceQuota \
    --enable-bootstrap-token-auth=true \
    --etcd-cafile=/var/lib/minikube/certs/etcd/ca.crt \
    --etcd-certfile=/var/lib/minikube/certs/apiserver-etcd-client.crt \
    --etcd-keyfile=/var/lib/minikube/certs/apiserver-etcd-client.key \
    --etcd-servers=https://127.0.0.1:2379 \
    --insecure-port=0 \
    --kubelet-client-certificate=/var/lib/minikube/certs/apiserver-kubelet-client.crt \
    --kubelet-client-key=/var/lib/minikube/certs/apiserver-kubelet-client.key \
    --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname \
    --proxy-client-cert-file=/var/lib/minikube/certs/front-proxy-client.crt \
    --proxy-client-key-file=/var/lib/minikube/certs/front-proxy-client.key \
    --requestheader-allowed-names=front-proxy-client \
    --requestheader-client-ca-file=/var/lib/minikube/certs/front-proxy-ca.crt \
    --requestheader-extra-headers-prefix=X-Remote-Extra- \
    --requestheader-group-headers=X-Remote-Group \
    --requestheader-username-headers=X-Remote-User \
    --secure-port=8443 \
    --service-account-key-file=/var/lib/minikube/certs/sa.pub \
    --service-cluster-ip-range=10.96.0.0/12 \
    --tls-cert-file=/var/lib/minikube/certs/apiserver.crt \
    --tls-private-key-file=/var/lib/minikube/certs/apiserver.key
```

Another way:

```bash
kubectl cluster-info dump | grep authorization-mode
                            "--authorization-mode=Node,RBAC",
```

:question: How to you show the authorization plugin that is in use?

[https://kubernetes.io/docs/reference/access-authn-authz/authorization/](https://kubernetes.io/docs/reference/access-authn-authz/authorization/)

[https://stackoverflow.com/questions/51238988/how-to-check-whether-rbac-is-enabled-using-kubectl](https://stackoverflow.com/questions/51238988/how-to-check-whether-rbac-is-enabled-using-kubectl)

Authorization options:

- `Node`: A special-purpose authorization mode that grants permissions to kubelets based on the pods they are scheduled to run. See [Node Authorization](https://kubernetes.io/docs/reference/access-authn-authz/node/).

### Creating Service Accounts and assigning Service Account to a Pod

Let's take a look at the default `ServiceAccount` in minikube:

```bash
k describe sa default
Name:                default
Namespace:           default
Labels:              <none>
Annotations:         <none>
Image pull secrets:  <none>
Mountable secrets:   default-token-6hbdw
Tokens:              default-token-6hbdw
Events:              <none>
```

Let's define a new ServiceAccount and use it in a Pod that uses the ambassador pattern to access the kubernetes API server.

1. Create a service account.

```
kubectl create serviceaccount foo
```

2. Define the following pod:

**examples/pods/sa-pod.yml**

```
apiVersion: v1
kind: Pod
metadata:
  name: curl-custom-sa
spec:
  serviceAccountName: foo
  containers:
  - name: main
    image: tutum/curl
    command: ["sleep", "9999999"]
  - name: ambassador
    image: luksa/kubectl-proxy:1.6.2
```

3. Create the Pod:

```
kubectl apply -f examples/pods/sa-pod.yml
```

4. Run the following command:

```
kubectl exec -it curl-custom-sa -c main -- cat /var/run/secrets/kubernetes.io/serviceaccount/token
eyJhbGciOiJSUzI1NiIsImtpZCI6Ikt5d2xSaTI4eWczZW4xQjNxNGFJdFRZbWh2RFhmdlByUlQxSzVpcV81U1EifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImZvby10b2tlbi1jbXJjaiIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJmb28iLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI5ZTFmYTBmNy0wY2ZhLTRmOGQtOTZjYy1hNjQzY2YwNDRjMDIiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6ZGVmYXVsdDpmb28ifQ.PeStECgss9KjMl2dFz6UMtbWLKmQP23TttAp_K9TnfYMfQZdX_DFNdOe2T_0Apr8H492klYN7_lfje_e_8B_mORLbyb8i79YhOLdG-CjSgz2MXlQtkb7bpUUGfX6HTQvxY8eROJG4x0lZij9K6vYmY0kRiUv_R-cJ5CJtSeN1pCQ38IJbwa5xJGXpe3MC5Dmq_-lgTA4u9jJ7tyc6cOX1P7yjo1b04jWWlxbQb0kustqaaG4PGmTLphgZf6rG0oJqCnawt9orxwLtp0kiXI5lala3IFGAHdIQmKd_h2zDo-N0aFASwpXqQNtZf31pw-pDdMchZLMPGJYDItA6QRFHA
```

5. Confirm that this token is the same one in the ServiceAccount's token:

```
kubectl describe sa foo
Name:                foo
Namespace:           default
Labels:              <none>
Annotations:         <none>
Image pull secrets:  <none>
Mountable secrets:   foo-token-cmrcj
Tokens:              foo-token-cmrcj
Events:              <none>
```

```
kubectl describe secret foo-token-cmrcj
Name:         foo-token-cmrcj
Namespace:    default
Labels:       <none>
Annotations:  kubernetes.io/service-account.name: foo
              kubernetes.io/service-account.uid: 9e1fa0f7-0cfa-4f8d-96cc-a643cf044c02

Type:  kubernetes.io/service-account-token

Data
====
ca.crt:     1066 bytes
namespace:  7 bytes
token:      eyJhbGciOiJSUzI1NiIsImtpZCI6Ikt5d2xSaTI4eWczZW4xQjNxNGFJdFRZbWh2RFhmdlByUlQxSzVpcV81U1EifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImZvby10b2tlbi1jbXJjaiIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJmb28iLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI5ZTFmYTBmNy0wY2ZhLTRmOGQtOTZjYy1hNjQzY2YwNDRjMDIiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6ZGVmYXVsdDpmb28ifQ.PeStECgss9KjMl2dFz6UMtbWLKmQP23TttAp_K9TnfYMfQZdX_DFNdOe2T_0Apr8H492klYN7_lfje_e_8B_mORLbyb8i79YhOLdG-CjSgz2MXlQtkb7bpUUGfX6HTQvxY8eROJG4x0lZij9K6vYmY0kRiUv_R-cJ5CJtSeN1pCQ38IJbwa5xJGXpe3MC5Dmq_-lgTA4u9jJ7tyc6cOX1P7yjo1b04jWWlxbQb0kustqaaG4PGmTLphgZf6rG0oJqCnawt9orxwLtp0kiXI5lala3IFGAHdIQmKd_h2zDo-N0aFASwpXqQNtZf31pw-pDdMchZLMPGJYDItA6QRFHA
```

6. Make a request to the Kubernetes API server:

```
kubectl exec -it curl-custom-sa -c main curl localhost:8001/api/v1/pods
{
  "kind": "Status",
  "apiVersion": "v1",
  "metadata": {

  },
  "status": "Failure",
  "message": "pods is forbidden: User \"system:serviceaccount:default:foo\" cannot list resource \"pods\" in API group \"\" at the cluster scope",
  "reason": "Forbidden",
  "details": {
    "kind": "pods"
  },
  "code": 403
}
```

:warning: Like we did in chapter 8, we need to disable RBAC, the second authorization mode used by `kube-apiserver`.

From the documentatoin on authorization:

> When multiple authorization modules are configured, each is checked in sequence. If any authorizer approves or denies a request, that decision is immediately returned and no other authorizer is consulted. If all modules have no opinion on the request, then the request is denied. A deny returns an HTTP status code 403.

To disable RBAC, run the following command:

```
kubectl create clusterrolebinding permissive-binding \
  --clusterrole=cluster-admin \
  --group=system:serviceaccounts
clusterrolebinding.rbac.authorization.k8s.io/permissive-binding created
```

*RBAC is covered in detail below*

Now run the command again and we see a response from the Kubernetes API server:

```
kubectl exec -it curl-custom-sa -c main curl localhost:8001/api/v1/pods
```

:white_check_mark:

#### A ServiceAccount's Mountable secrets

By default, a Pod can mount any `Secret` it wants. A Pod's `ServiceAccount` can be configured to only allow the Pod to mount secrets that are listed as mountable secrets in the `ServiceAccount`.

To enable this, include the following annotation on the in the `ServiceAccount`:

```
kubernetes.io/enforce-mountable-secrets="true"
```

#### `imagePullSecrets`

This is a list of secrets that are used to authenticate to a private image registries.

::: tip About ServiceAccounts and RBAC
When your Kubernetes cluster is not using proper authorization, using ServiceAccounts would only be used to enforce Mountable secrets or to provide image pull secrets.
:::

## Securing the cluster with role-based access control (RBAC)

Kubernetes 1.6.0 improved security significantly.

> RBAC prevents unauthorized users from viewing or modifying the cluster state. The default ServiceAccount isn’t allowed to view cluster state, let alone modify it in any way, unless you
grant it additional privileges.

### The RBAC authorization plugin

The RBAC plugin runs inside of the API server and determines whether or not a client is allowed to take perform a requested verb (`GET`, `POST`, `DELETE`), on a requested Kubernetes API resource.

`RBAC` rules can apply to an entire resource type, or a specific instance of a resource type, such as a service called `myservice`.

Some RBAC rules can apply to **non-resource** URL paths (such as `/healthz`, or `/api`)

#### Understanding the RBAC plugin

RBAC uses **user roles** to determine if a user can do something with the Kubernetes API or not.

A `Subject`:

- human
- `ServiceAccount`
- group of users
- group of `ServiceAccount`s

is associated with one or more `roles`. Each `role` is allowed to perform certain verbs on certain resources.

### RBAC Resources

RBAC authorization rules are configured through four resources:

- `Roles` and `ClusterRoles`: specify which verbs can be performed on which resources
- `RoleBindings` and `ClusterRoleBindings`: Bind `Roles` and `CluterRoles` to specific **users**, **groups** or **ServiceAccounts**

<base-image width="100%" image="rbac.png" />

*Roles grant permissions, RoleBindings bind Roles to subjects*

- `Role` and `RoleBinding` are **namespaced** resources
- `ClusterRole` and `ClusterRoleBinding` are **cluster-level** resources

#### Setting up the Exercise

Requirements:

- Kubernetes >1.6
- RBAC must be the **only** authorization method

::: warning Multiple Authorization Plugins
There can be multiple plugins enabled in parallel and if one of them allows an action to be performed, the action is allowed.
:::

Start minikube with:

~~minikube start --extra-config=apiserver.Authorization.Mode=RBAC~~
```
minikube start --extra-config=apiserver.authorization-mode=RBAC
```

We also need to remove the `ClusterRoleBinding` that we created earlier. Recall that it was called `permissive-binding`:

```
kubectl get clusterrolebindings | grep permissive
permissive-binding
```

```
kubectl describe clusterrolebinding permissive-binding
Name:         permissive-binding
Labels:       <none>
Annotations:  <none>
Role:
  Kind:  ClusterRole
  Name:  cluster-admin
Subjects:
  Kind   Name                    Namespace
  ----   ----                    ---------
  Group  system:serviceaccounts
```

This `ClusterRoleBinding` gives the `cluster-admin` ClusterRole to the built-in `system:serviceaccounts` group, which includes all `ServiceAccount`s in the cluster.

Let's look at the `cluster-admin` role:

```
k describe clusterrole cluster-admin
Name:         cluster-admin
Labels:       kubernetes.io/bootstrapping=rbac-defaults
Annotations:  rbac.authorization.kubernetes.io/autoupdate: true
PolicyRule:
  Resources  Non-Resource URLs  Resource Names  Verbs
  ---------  -----------------  --------------  -----
  *.*        []                 []              [*]
             [*]                []              [*]
```

This ClusterRole allows all verbs on all resources and non-resource URLs, so it grants a high level of permission. This is why this should never be done in production.

Let's delete this `ClusterRoleBinding`:

```
kubectl delete clusterrolebinding permissive-binding
```

Restart minikube with the RBAC option, and verify that RBAC is the only authorization mode enabled:

```
kubectl cluster-info dump | grep authorization-mode
```

```
minikube start --extra-config=apiserver.authorization-mode=RBAC
```

::: warning Can't run RBAC only in minikube
Open issue: [https://github.com/kubernetes/minikube/issues/1722](https://github.com/kubernetes/minikube/issues/1722)
:::

### Using Roles and RoleBindings

<base-image width="100%" image="role1.png" />

<base-image width="100%" image="role2.png" />

### User ClusterRoles and ClusterRoleBindings

<base-image width="100%" image="clusterrole1.png" />

<base-image width="100%" image="clusterrole2.png" />

### Understanding default ClusterRoles and ClusterRoleBindings

### Granting authorization permissions

## Summary

- Clients of the API server include humans and applications running in pods
- Applications in pods are associated with e **service account**
- both **users** and **ServiceAccounts** are associated with groups
- pods run under the default **ServiceAccount**, which is created for each namespace automatically
- Additional **ServiceAccounts** can be created manually and associated with a pod
- **ServiceAccounts** can be configured to allow mounting only a constrained list of Secrets in a given pod
- A **ServiceAccount** can also be used to attach image pull Secrets to pods, these Secrets don't need to be specified in every pod
- Roles and ClusterRoles define what actions can be performed on which resources
- RoleBindings and ClusterRoleBindings bind Roles and ClusterRoles to **users**, **groups** and **ServiceAccounts**
- Each cluster comes with default ClusterRoles and ClusterRoleBindings
