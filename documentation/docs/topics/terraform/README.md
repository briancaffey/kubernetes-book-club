# Setup a GKE Cluster with Terraform

Using this guide: [https://elastisys.com/2019/04/12/kubernetes-on-gke-from-scratch-using-terraform/](https://elastisys.com/2019/04/12/kubernetes-on-gke-from-scratch-using-terraform/)


## Setup

### Create a project

### Enable GKE API

Visit [this link](https://console.developers.google.com/apis/api/container.googleapis.com/overview) to enable the GKE API.

### Create Service Account for Terraform

Select `IAM` > `Service Accounts` and create a Service Account with name:

```
terraform
```

Next, assign the following Roles to the service account:

1. `Kubernetes Engine Admin` (Full management of Kubernetes Clusters and their Kubernetes API objects)
1. Storage Admin (for Google Cloud Storage, because Terraform uses a bucket to store state)

Finally, create the JSON key and save it.

### Create Google Cloud Storage Bucket

This bucket will store the Terraform state.

Create a bucket called `terraform-gke-test`, and then select all of the default options for the bucket.

Run the following commands:

```
export BUCKET_ID=terraform-gke-test
gsutil versioning set on gs://${BUCKET_ID}
Enabling versioning for gs://terraform-gke-test/...
gsutil versioning get gs://${BUCKET_ID}
gs://terraform-gke-test: Enabled
```

### Configuring Google Cloud Storage as Terraform remote state

Setup Terraform workspace for each of our deployments (staging, production, etc.)

`google.tf` has one hard-coded value where we specify the bucket name we are using.

Remove the prefix from the terraform backend "gcs" resource since we are not using this.

### Configure Terraform

```
export ENVIRONMENT=production
terraform workspace new ${ENVIRONMENT}
Created and switched to workspace "production"!

You're now on a new, empty workspace. Workspaces isolate their state,
so if you run "terraform plan" Terraform will not see any existing state
for this configuration.
```

Next, run:

```
terraform init -var-file=${ENVIRONMENT}.tfvars

Initializing the backend...

Initializing provider plugins...

The following providers do not have any version constraints in configuration,
so the latest version was installed.

To prevent automatic upgrades to new major versions that may contain breaking
changes, it is recommended to add version = "..." constraints to the
corresponding provider blocks in configuration, with the constraint strings
suggested below.

* provider.google: version = "~> 2.17"

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

### Using Terraform to create a GKE cluster

Setup a GKE with an *initial node pool* that it will delete immediately and then setup a new one so that we can manage it (auto-upgrades, auto-repairs, auto-scaling).

This is so common (and the recommended approach) that Terraform has a special option for it.

### Giving our Service Account required permissions

We need to give our service account more permissions. If we tried to deploy now, there would be errors. To do this, run:

```
export PROJECT=verbose-equals-t-1544500603845
export SERVICE_ACCOUNT=terraform
gcloud projects add-iam-policy-binding ${PROJECT} --member serviceAccount:${SERVICE_ACCOUNT}@${PROJECT}.iam.gserviceaccount.com --
role roles/editor

Updated IAM policy for project [verbose-equals-t-1544500603845].
bindings:
- members:
  - serviceAccount:service-855558823208@compute-system.iam.gserviceaccount.com
  role: roles/compute.serviceAgent
- members:
  - serviceAccount:terraform@verbose-equals-t-1544500603845.iam.gserviceaccount.com
  role: roles/container.admin
- members:
  - serviceAccount:855558823208-compute@developer.gserviceaccount.com
  - serviceAccount:855558823208@cloudservices.gserviceaccount.com
  - serviceAccount:service-855558823208@containerregistry.iam.gserviceaccount.com
  - serviceAccount:terraform@verbose-equals-t-1544500603845.iam.gserviceaccount.com
  role: roles/editor
- members:
  - user:briancaffey2010@gmail.com
  role: roles/owner
- members:
  - serviceAccount:terraform@verbose-equals-t-1544500603845.iam.gserviceaccount.com
  role: roles/storage.admin
etag: BwWUu-bTEEY=
version: 1
```

### Deploying the GKE cluster

```
terraform apply -var-file=${ENVIRONMENT}.tfvars
```

```
terraform apply -var-file=${ENVIRONMENT}.tfvars

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # google_container_cluster.cluster will be created
  + resource "google_container_cluster" "cluster" {
      + additional_zones            = (known after apply)
      + cluster_autoscaling         = (known after apply)
      + cluster_ipv4_cidr           = (known after apply)
      + default_max_pods_per_node   = (known after apply)
      + enable_binary_authorization = (known after apply)
      + enable_kubernetes_alpha     = false
      + enable_legacy_abac          = false
      + enable_tpu                  = (known after apply)
      + endpoint                    = (known after apply)
      + id                          = (known after apply)
      + initial_node_count          = 1
      + instance_group_urls         = (known after apply)
      + ip_allocation_policy        = (known after apply)
      + location                    = "us-central1"
      + logging_service             = (known after apply)
      + master_version              = (known after apply)
      + monitoring_service          = (known after apply)
      + name                        = "verbose-equals-t-1544500603845-cluster"
      + network                     = "default"
      + node_locations              = (known after apply)
      + node_version                = (known after apply)
      + project                     = (known after apply)
      + region                      = (known after apply)
      + remove_default_node_pool    = true
      + services_ipv4_cidr          = (known after apply)
      + subnetwork                  = (known after apply)
      + zone                        = (known after apply)

      + addons_config {
          + horizontal_pod_autoscaling {
              + disabled = (known after apply)
            }

          + http_load_balancing {
              + disabled = (known after apply)
            }

          + kubernetes_dashboard {
              + disabled = (known after apply)
            }

          + network_policy_config {
              + disabled = false
            }
        }

      + master_auth {
          + client_certificate     = (known after apply)
          + client_key             = (sensitive value)
          + cluster_ca_certificate = (known after apply)

          + client_certificate_config {
              + issue_client_certificate = (known after apply)
            }
        }

      + network_policy {
          + enabled  = true
          + provider = "CALICO"
        }

      + node_config {
          + disk_size_gb      = (known after apply)
          + disk_type         = (known after apply)
          + guest_accelerator = (known after apply)
          + image_type        = (known after apply)
          + labels            = (known after apply)
          + local_ssd_count   = (known after apply)
          + machine_type      = (known after apply)
          + metadata          = (known after apply)
          + min_cpu_platform  = (known after apply)
          + oauth_scopes      = (known after apply)
          + preemptible       = (known after apply)
          + service_account   = (known after apply)
          + tags              = (known after apply)

          + sandbox_config {
              + sandbox_type = (known after apply)
            }

          + taint {
              + effect = (known after apply)
              + key    = (known after apply)
              + value  = (known after apply)
            }

          + workload_metadata_config {
              + node_metadata = (known after apply)
            }
        }

      + node_pool {
          + initial_node_count  = (known after apply)
          + instance_group_urls = (known after apply)
          + max_pods_per_node   = (known after apply)
          + name                = (known after apply)
          + name_prefix         = (known after apply)
          + node_count          = (known after apply)
          + version             = (known after apply)

          + autoscaling {
              + max_node_count = (known after apply)
              + min_node_count = (known after apply)
            }

          + management {
              + auto_repair  = (known after apply)
              + auto_upgrade = (known after apply)
            }

          + node_config {
              + disk_size_gb      = (known after apply)
              + disk_type         = (known after apply)
              + guest_accelerator = (known after apply)
              + image_type        = (known after apply)
              + labels            = (known after apply)
              + local_ssd_count   = (known after apply)
              + machine_type      = (known after apply)
              + metadata          = (known after apply)
              + min_cpu_platform  = (known after apply)
              + oauth_scopes      = (known after apply)
              + preemptible       = (known after apply)
              + service_account   = (known after apply)
              + tags              = (known after apply)

              + sandbox_config {
                  + sandbox_type = (known after apply)
                }

              + taint {
                  + effect = (known after apply)
                  + key    = (known after apply)
                  + value  = (known after apply)
                }

              + workload_metadata_config {
                  + node_metadata = (known after apply)
                }
            }
        }
    }

  # google_container_node_pool.general_purpose will be created
  + resource "google_container_node_pool" "general_purpose" {
      + cluster             = "verbose-equals-t-1544500603845-cluster"
      + id                  = (known after apply)
      + initial_node_count  = 1
      + instance_group_urls = (known after apply)
      + location            = "us-central1"
      + max_pods_per_node   = (known after apply)
      + name                = "verbose-equals-t-1544500603845-general"
      + name_prefix         = (known after apply)
      + node_count          = (known after apply)
      + project             = (known after apply)
      + region              = (known after apply)
      + version             = (known after apply)
      + zone                = (known after apply)

      + autoscaling {
          + max_node_count = 3
          + min_node_count = 1
        }

      + management {
          + auto_repair  = true
          + auto_upgrade = true
        }

      + node_config {
          + disk_size_gb      = (known after apply)
          + disk_type         = (known after apply)
          + guest_accelerator = (known after apply)
          + image_type        = (known after apply)
          + labels            = (known after apply)
          + local_ssd_count   = (known after apply)
          + machine_type      = "n1-standard-1"
          + metadata          = {
              + "disable-legacy-endpoints" = "true"
            }
          + oauth_scopes      = [
              + "https://www.googleapis.com/auth/devstorage.read_only",
              + "https://www.googleapis.com/auth/logging.write",
              + "https://www.googleapis.com/auth/monitoring",
            ]
          + preemptible       = false
          + service_account   = (known after apply)
        }
    }

Plan: 2 to add, 0 to change, 0 to destroy.

Do you want to perform these actions in workspace "production"?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value:
```

Errors:

```
  Enter a value: yes

google_container_cluster.cluster: Creating...
google_container_cluster.cluster: Still creating... [10s elapsed]
google_container_cluster.cluster: Still creating... [20s elapsed]
google_container_cluster.cluster: Still creating... [30s elapsed]
google_container_cluster.cluster: Still creating... [40s elapsed]
google_container_cluster.cluster: Still creating... [50s elapsed]
google_container_cluster.cluster: Still creating... [1m0s elapsed]

Error: googleapi: Error 500: Internal error encountered., backendError

  on cluster.tf line 18, in resource "google_container_cluster" "cluster":
  18: resource "google_container_cluster" "cluster" {


```

[https://github.com/hashicorp/terraform/issues/20029](https://github.com/hashicorp/terraform/issues/20029)