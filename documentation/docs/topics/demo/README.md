# Demo application

Here's an overview of of how to start an application on minikube. A more detailed explanation can be found here: [https://verbose-equals-true.gitlab.io/django-postgres-vue-gitlab-ecs/topics/minikube/](https://verbose-equals-true.gitlab.io/django-postgres-vue-gitlab-ecs/topics/minikube/)

## Project setup

Here is how to setup the entire application from a fresh minikube Kubernetes cluster.

### Prepare minikube

Delete any existing minikube cluster and then start a new one:

```
minikube delete
minikube start
```

### Edit /etc/hosts

Get the minikube Kubernetes cluster IP with the following command:

```
minikube ip
192.168.99.108
```

Edit your `/etc/hosts` file and add an entry to map `minikube.local` (or any other domain you want to use) to the result of `minikube ip`:

```
sudo vim /etc/hosts
127.0.0.1       localhost
127.0.1.1       a1
192.168.99.108  minikube.local <-- this is the line you need to add
# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

### Enable ingress addon

Then enable the ingress adddon:

```
minikube addons enable ingress
```

### Build docker images

Next, set your local docker CLI to point to the minikube docker daemon:

```
eval $(minikube docker-env)
```

Next, build the frontend and backend containers with the following command:

```
docker-compose -f compose/minikube.yml build frontend backend
```

### Configure Kubernetes resources

```
k apply -f kubernetes/postgres/
k apply -f kubernetes/redis/
k apply -f kubernetes/django/
k apply -f kubernetes/channels/
k apply -f kubernetes/celery/
k apply -f kubernetes/beat/
k apply -f kubernetes/frontend/
k apply -f kubernetes/flower/
k apply -f kubernetes/ingress.yml
```

Check that you can visit `minikube.local` in your browser.

