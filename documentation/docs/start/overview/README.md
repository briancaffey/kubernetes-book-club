---
prev: false
---

# Start Here

[Chapter 3: `Pods`](/topics/pods)

Pods are a fundemental topic in kubernetes. This is an introduction to creating, organizing and deleting pods.