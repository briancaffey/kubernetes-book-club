# Tools used in this project

This is a list of the tools used in this project.


<base-image image="kubernetes.png" />

## [Kubernetes](https://www.kubernetes.io/)

### Production-Grade Container Orchestration

Kubernetes (K8s) is an open-source system for automating deployment, scaling, and management of containerized applications.

It groups containers that make up an application into logical units for easy management and discovery. Kubernetes builds upon 15 years of experience of running production workloads at Google, combined with best-of-breed ideas and practices from the community.


<base-image image="vuepress.png" />

## [VuePress.js](https://vuepress.vuejs.org/)

### Vue-powered Static Site Generator

