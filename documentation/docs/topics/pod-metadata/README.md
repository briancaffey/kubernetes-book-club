# Pod Metadata

This chapters covers two main topics:

1) Using the `Downward API` to pass Pod metadata to containers

2) Different ways to access the Kubernetes API from
    - Your containerized application
    - localhost

## Downward API

The Downward API allows us to access metadata of the Pod in which our container is running.

The following information is available through the Downard API:

- The pod's name
- The pod's IP address
- The namespace the pod belongs to
- Name of the node the pod is running on
- Name of the service account the pod is running under
- CPU and memory requests for each container
- CPU and memory limits for each container
- The pod's labels
- The pod's annotations

This data can be accessed through volumes, and all data but labels and annotations can be accessed through environment variables.

::: tip Experiment
Show how to get all Downward API metadata through both env vars and volumes.
:::

To demonstrate how we can access Downward API data in our application, we can add environment variables to our Django deployment:

```yml
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name

            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
```

Here are two examples of how we access Pod metadata through environment variables.

Let's add label and annotation data with a volume mount:

```yml
          volumeMounts:
            - name: downward
              mountPath: /etc/downward

      volumes:
        - name: downward
          downwardAPI:
            items:
            - path: "labels"
              fieldRef:
                fieldPath: metadata.labels
            - path: "annotations"
              fieldRef:
                fieldPath: metadata.annotations
```

Next, let's add an endpoint that returns pod metadata from environment variables and our `downward` volume.

```python
def downward_api(request):
    pod_name = os.environ.get("POD_NAME", "<pod name")
    pod_namespace = os.environ.get("POD_NAMESPACE", "<pod namespace")
    pod_ip = os.environ.get("POD_IP", "<pod ip>")
    node_name = os.environ.get("NODE_NAME", "<node name>")
    service_account = os.environ.get("SERVICE_ACCOUNT", "<service account>")
    container_cpu_request_millicores = \
        os.environ.get("CONTAINER_CPU_REQUEST_MILLICORES", "<container cpu request m>")
    container_memory_limit = \
        os.environ.get("CONTAINER_MEMORY_LIMIT_KIBIBYTES", "<container memory request m>")

    try:
        with open("/etc/downward/labels") as f:
            labels = {k:v for k, v in (l.split('=') for l in f)}
    except FileNotFoundError:
        labels = []

    try:
        with open("/etc/downward/annotations") as f:
            annotations = {k:v for k, v in (l.split('=') for l in f)}
    except FileNotFoundError:
        annotations = []

    return JsonResponse({
        "pod_name": pod_name,
        "pod_namespace": pod_namespace,
        "pod_ip": pod_ip,
        "node_name": node_name,
        "service_account": service_account,
        "container_cpu_request_millicores": container_cpu_request_millicores,
        "container_memory_limit": container_memory_limit,
        "labels": labels,
        "annotations": annotations
    })
```

When we visit a url that calls this function, we see:

```json
{
    "pod_name": "django-8455fb847c-l69sw",
    "pod_namespace": "default",
    "pod_ip": "172.17.0.15",
    "node_name": "minikube",
    "service_account": "default",
    "container_cpu_request_millicores": "1000",
    "labels": {
        "app": "\"django-container\"\n",
        "pod-template-hash": "\"8455fb847c\""
    },
    "annotations": {
        "kubernetes.io/config.seen": "\"2019-09-28T19:09:48.357111674Z\"\n",
        "kubernetes.io/config.source": "\"api\""
    }
}
```

Formatting is a little bit messed up, but you can see that Pod metadata is being accessed through both the mounted volume and the environment variables that were configured in the Pod template.

Notice that there is an automatically generated label called `pod-template-hash`. This is a hash of the pod template.

Here's the full deployment that shows how a pod can access the downward API:

```yml
apiVersion: apps/v1beta2
kind: Deployment
metadata:
  name: django
spec:
  replicas: 1
  selector:
    matchLabels:
      app: django-container
  template:
    metadata:
      labels:
        app: django-container
    spec:
      containers:
        - name: backend
          imagePullPolicy: IfNotPresent
          image: backend:17
          command: ["./manage.py", "runserver", "0.0.0.0:8000"]
          resources:
            requests:
              cpu: "1"
              memory: 1Gi
            limits:
              cpu: 1500m
              memory: 2Gi
          livenessProbe:
            httpGet:
              path: /healthz
              port: "8000"
          readinessProbe:
            # an http probe
            httpGet:
              path: /readiness
              port: "8000"
            initialDelaySeconds: 10
            timeoutSeconds: 5
          ports:
          - containerPort: 8000
          env:
            - name: DJANGO_SETTINGS_MODULE
              value: 'backend.settings.minikube'

            - name: SECRET_KEY
              value: "my-secret-key"

            - name: POSTGRES_NAME
              value: postgres

            - name: POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: postgres-credentials
                  key: user

            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: postgres-credentials
                  key: password

            # Downward API data

            - name: POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name

            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace

            - name: POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP

            - name: NODE_NAME
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName

            - name: SERVICE_ACCOUNT
              valueFrom:
                fieldRef:
                  fieldPath: spec.serviceAccountName

            - name: CONTAINER_CPU_REQUEST_MILLICORES
              valueFrom:
                resourceFieldRef:
                  resource: requests.cpu
                  divisor: 1m

            - name: CONTAINER_MEMORY_LIMIT_KIBIBYTES
              valueFrom:
                resourceFieldRef:
                  resource: limits.memory
                  divisor: 1Ki

          volumeMounts:
            - name: downward
              mountPath: /etc/downward

      volumes:
        - name: downward
          downwardAPI:
            items:
            - path: "labels"
              fieldRef:
                fieldPath: metadata.labels
            - path: "annotations"
              fieldRef:
                fieldPath: metadata.annotations
```

::: warning Note
Accessing container-specific information though the volume requires that you specify a container with the `containerName` field.
:::

## Kubernetes API

`kubectl cluster-info` will give us the address of the Kubernetes API server:

```
kubectl cluster-info

Kubernetes master is running at https://192.168.99.108:8443
KubeDNS is running at https://192.168.99.108:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

Curling the address of the kubernetes API server directly gives us the following:

```
curl https://192.168.99.108:8443 -k
{
  "kind": "Status",
  "apiVersion": "v1",
  "metadata": {

  },
  "status": "Failure",
  "message": "forbidden: User \"system:anonymous\" cannot get path \"/\"",
  "reason": "Forbidden",
  "details": {

  },
  "code": 403
}
```

::: tip
The book showed that this request would give a response showing `unauthorized`.
:::

To get around this, we can run `kubectl proxy` and access the kubernetes API server from our `localhost`:

```
kubectl proxy
Starting to serve on 127.0.0.1:8001
```

Now, visiting `localhost:8001` gives us the following response:


```json
{
    "paths": [
        "/api",
        "/api/v1",
        "/apis",
        "/apis/",
        "/apis/admissionregistration.k8s.io",
        "/apis/admissionregistration.k8s.io/v1beta1",
...
```

We can inspect the `jobs` endpoint (http://localhost:8001/apis/batch/v1/jobs):

```json
{
    "kind": "JobList",
    "apiVersion": "batch/v1",
    "metadata": {
        "selfLink": "/apis/batch/v1/jobs",
        "resourceVersion": "13009"
    },
    "items": [{
        "metadata": {
            "name": "django-migrations",
            "namespace": "default",
            "selfLink": "/apis/batch/v1/namespaces/default/jobs/django-migrations",
            "uid": "956adedc-7585-4c1d-9c0c-609dd7e32142",
            "resourceVersion": "2199",
            "creationTimestamp": "2019-09-28T17:41:02Z",
            "labels": {
                "controller-uid": "956adedc-7585-4c1d-9c0c-609dd7e32142",
                "job-name": "django-migrations"
            },
            "annotations": {
                ...
```

### Talking to the API server from within a Pod

To access the Kubernetes API server from within a pod, there are three things that we need to do:

1. Find the location of the API server
1. Make sure you are talking to the API server and not something that is pretending to be the API server.
1. Authenticate with the server so that we can do things with the Kubernetes API

The kubernetes API server is accessible over DNS and also through environment variables. Run the following command to see the kubernetes service:

```
kubectl get svc
NAME                                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
flower-service                       NodePort    10.97.24.91     <none>        5555:32269/TCP   115m
kubernetes                           ClusterIP   10.96.0.1       <none>        443/TCP          138m
kubernetes-django-channels-service   NodePort    10.108.31.30    <none>        9000:32511/TCP   118m
kubernetes-django-service            NodePort    10.107.150.86   <none>        8000:31636/TCP   121m
kubernetes-frontend-service          NodePort    10.107.69.29    <none>        80:30002/TCP     115m
postgres                             ClusterIP   10.99.17.50     <none>        5432/TCP         135m
redis                                ClusterIP   10.105.48.73    <none>        6379/TCP         133m
```

Notice the kubernetes service with the name `kubernetes`. We can also see this data in our environment variables

This allows us to find the kuberenetes service by accessing `https://kubernetes`, or by using the following environment variables:

```
k exec django-8455fb847c-l69sw env | grep KUBERNETES_SERVICE
KUBERNETES_SERVICE_PORT=443
KUBERNETES_SERVICE_PORT_HTTPS=443
KUBERNETES_SERVICE_HOST=10.96.0.1
```

Let's try accessing the Kubernetes API service through DNS by curling `https://kubernetes` from inside of one of our containers. First, shell into a container from our Django deployment:

```
kubectl exec django-8455fb847c-l69sw -it -- bash
```

Then, run the following command from inside of the container:

```
root@django-8455fb847c-l69sw:/code# curl https://kubernetes
curl: (60) SSL certificate problem: unable to get local issuer certificate
More details here: https://curl.haxx.se/docs/sslcerts.html

curl failed to verify the legitimacy of the server and therefore could not
establish a secure connection to it. To learn more about this situation and
how to fix it, please visit the web page mentioned above.
```
Try passing the `-k` option:

```
root@django-8455fb847c-l69sw:/code# curl https://kubernetes -k
{
  "kind": "Status",
  "apiVersion": "v1",
  "metadata": {

  },
  "status": "Failure",
  "message": "forbidden: User \"system:anonymous\" cannot get path \"/\"",
  "reason": "Forbidden",
  "details": {

  },
  "code": 403
}
```

Using the `--cacert` is not working:

```
curl --cacert /var/run/secrets/kubernetes.io/serviceaccount/ca.crt https://kubernetes
{
  "kind": "Status",
  "apiVersion": "v1",
  "metadata": {

  },
  "status": "Failure",
  "message": "forbidden: User \"system:anonymous\" cannot get path \"/\"",
  "reason": "Forbidden",
  "details": {

  },
  "code": 403
}
```

### Disabling role-based access control (RBAC)

Run the following command to disable RBAC:

```
kubectl create clusterrolebinding permissive-binding \
--clusterrole=cluster-admin \
--group=system:serviceaccounts
```

::: danger Don't do this in production
This gives all service accounts (we could also say all pods) cluster-admin privileges,
:::

Set the `CURL_CA_BUNDLE` environment variable inside of the pod:

```
export CURL_CA_BUNDLE=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt
```

Set the `TOKEN`:

```
TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
```

Now run curl with passing in the `$TOKEN` as an Authorization token:

```
root@django-8455fb847c-sv2kg:/code# curl -H "Authorization: Bearer $TOKEN" https://kubernetes
{
  "paths": [
    "/api",
    "/api/v1",
    "/apis",
    "/apis/",
    "/apis/admissionregistration.k8s.io",
    "/apis/admissionregistration.k8s.io/v1beta1",
    "/apis/apiextensions.k8s.io",
    "/apis/apiextensions.k8s.io/v1beta1",
    "/apis/apiregistration.k8s.io",
    "/apis/apiregistration.k8s.io/v1",
    "/apis/apiregistration.k8s.io/v1beta1",
    "/apis/apps",
    "/apis/apps/v1",
    "/apis/apps/v1beta1",
    "/apis/apps/v1beta2",
    "/apis/authentication.k8s.io",
    "/apis/authentication.k8s.io/v1",
    "/apis/authentication.k8s.io/v1beta1",
    "/apis/authorization.k8s.io",
    "/apis/authorization.k8s.io/v1",
    "/apis/authorization.k8s.io/v1beta1",
    "/apis/autoscaling",
    "/apis/autoscaling/v1",
    "/apis/autoscaling/v2beta1",
    "/apis/autoscaling/v2beta2",
    "/apis/batch",
    "/apis/batch/v1",
    "/apis/batch/v1beta1",
    "/apis/certificates.k8s.io",
    "/apis/certificates.k8s.io/v1beta1",
    "/apis/coordination.k8s.io",
    "/apis/coordination.k8s.io/v1",
    "/apis/coordination.k8s.io/v1beta1",
    "/apis/events.k8s.io",
    "/apis/events.k8s.io/v1beta1",
    "/apis/extensions",
    "/apis/extensions/v1beta1",
    "/apis/networking.k8s.io",
    "/apis/networking.k8s.io/v1",
    "/apis/networking.k8s.io/v1beta1",
    "/apis/node.k8s.io",
    "/apis/node.k8s.io/v1beta1",
    "/apis/policy",
    "/apis/policy/v1beta1",
    "/apis/rbac.authorization.k8s.io",
    "/apis/rbac.authorization.k8s.io/v1",
    "/apis/rbac.authorization.k8s.io/v1beta1",
    "/apis/scheduling.k8s.io",
    "/apis/scheduling.k8s.io/v1",
    "/apis/scheduling.k8s.io/v1beta1",
    "/apis/storage.k8s.io",
    "/apis/storage.k8s.io/v1",
    "/apis/storage.k8s.io/v1beta1",
    "/healthz",
    "/healthz/autoregister-completion",
    "/healthz/etcd",
    "/healthz/log",
    "/healthz/ping",
    "/healthz/poststarthook/apiservice-openapi-controller",
    "/healthz/poststarthook/apiservice-registration-controller",
    "/healthz/poststarthook/apiservice-status-available-controller",
    "/healthz/poststarthook/bootstrap-controller",
    "/healthz/poststarthook/ca-registration",
    "/healthz/poststarthook/crd-informer-synced",
    "/healthz/poststarthook/generic-apiserver-start-informers",
    "/healthz/poststarthook/kube-apiserver-autoregistration",
    "/healthz/poststarthook/rbac/bootstrap-roles",
    "/healthz/poststarthook/scheduling/bootstrap-system-priority-classes",
    "/healthz/poststarthook/start-apiextensions-controllers",
    "/healthz/poststarthook/start-apiextensions-informers",
    "/healthz/poststarthook/start-kube-aggregator-informers",
    "/healthz/poststarthook/start-kube-apiserver-admission-initializer",
    "/logs",
    "/metrics",
    "/openapi/v2",
    "/version"
  ]
}
```

<base-image width="100%" image="k8s-api.png" />

The kubernetes documentation describes one other way to hit the Kubernetes API from our `localhost` without using `kubectl proxy` [here](https://kubernetes.io/docs/tasks/administer-cluster/access-cluster-api/):

```bash
# Check all possible clusters, as you .KUBECONFIG may have multiple contexts:
kubectl config view -o jsonpath='{"Cluster name\tServer\n"}{range .clusters[*]}{.name}{"\t"}{.cluster.server}{"\n"}{end}'

# Select name of cluster you want to interact with from above output:
export CLUSTER_NAME="some_server_name"

# Point to the API server refering the cluster name
APISERVER=$(kubectl config view -o jsonpath="{.clusters[?(@.name==\"$CLUSTER_NAME\")].cluster.server}")

# Gets the token value
TOKEN=$(kubectl get secrets -o jsonpath="{.items[?(@.metadata.annotations['kubernetes\.io/service-account\.name']=='default')].data.token}"|base64 -d)

# Explore the API with TOKEN
curl -X GET $APISERVER/api --header "Authorization: Bearer $TOKEN" --insecure
```

### Ambassador Pattern

The book shows how you can use another container in the pod to handle authentication to the server. The container is defined by a simple dockerfile and one script:

**Dockerfile**

```bash
FROM alpine
RUN apk update \
    && apk add curl \
    && curl -L -O https://dl.k8s.io/v1.8.0/kubernetes-client-linux-amd64.tar.gz \
    && tar zvxf kubernetes-client-linux-amd64.tar.gz kubernetes/client/bin/kubectl \
    && mv kubernetes/client/bin/kubectl / \
    && rm -rf kubernetes \
    && rm -f kubernetes-client-linux-amd64.tar.gz
ADD kubectl-proxy.sh /kubectl-proxy.sh
ENTRYPOINT /kubectl-proxy.sh
```

**kubectl-proxy.sh**

```bash
#!/bin/sh

API_SERVER="https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_SERVICE_PORT"
CA_CRT="/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
TOKEN="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)"

/kubectl proxy --server="$API_SERVER" --certificate-authority="$CA_CRT" --token="$TOKEN" --accept-paths='^.*'
```

::: warning Change file permissions
Make sure that you make the `kubectl-proxy.sh` executable by running:

```
sudo chmod +x ambassador/kubectl-proxy.sh
```

Otherwise, the container will not start properly.

:::

This works! We can shell into the container with the following command:

```
k exec django-56d954fb94-mrbnq -it -c backend -- bash
```

And then we can curl `localhost:8001` and we should see a response from the kubernetes API.

## Swagger API

Launch minikube with the following command:

```
minikube start --extra-config=apiserver.Features.Enable-SwaggerUI=true
```
