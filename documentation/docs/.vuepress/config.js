const path = require("path");

module.exports = {
  title: "Kubernetes Book Club",
  base: "/kubernetes-book-club/",
  port: 8080,
  configureWebpack: {
    resolve: {
      alias: {
        "@assets": path.resolve(__dirname, "../assets")
      }
    }
  },
  dest: "../public",
  serviceWorker: false,
  themeConfig: {
    lastUpdated: "Last Updated: ",
    sidebar: "auto",
    repo: process.env.CI_PROJECT_URL,
    docsBranch: "master",
    repoLabel: "View on GitLab",
    docsDir: "documentation/docs",
    editLinks: true,
    editLinkText: "Edit this page on GitLab",
    nav: [
      { text: "Home", link: "/" },
      {
        text: "Start Here",
        items: [
          { text: "Overview", link: "/start/overview/" },
          { text: "Tools Used", link: "/start/tools/" }
        ]
      },
      {
        text: "Topics",
        items: [
          { text: "Pods", link: "/topics/pods/" },
          { text: "Pod Metadata", link: "/topics/pod-metadata/" },
          { text: "Demo Application", link: "/topics/demo/" },
          {
            text: "Securing the K8s API server",
            link: "/topics/securing-the-api-server/"
          },
          {
            text: "Terraform",
            link: "/topics/terraform/"
          },
          {
            text: "Resource Limits",
            link: "/topics/resource-limits/"
          }
        ]
      },
      {
        text: "Source Code",
        link: "https://gitlab.com/briancaffey/kubernetes-book-club"
      }
    ]
  }
};
